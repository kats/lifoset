#include "lifoset.h"

template <class Data>
inline
Data & Lifoset<Data>::insert(const Data & data)
{
  Node * inserted(0);
  if ( _root == 0 ) {
    inserted = _root = make_node(data);
  } else {
    Node head;
    Node * gpar, * tmp, * par, * it;
    uint direction = 0, last;
    
    tmp = &head;
    gpar = par = 0;
    it = tmp->_kids[1] = _root;
    while ( true ){
      if ( it == 0 ){
        // new node
        par->_kids[direction] = it = make_node(data);
      } else if ( is_red(it->_kids[0]) && is_red(it->_kids[1]) ){
        // color flip
        it->_red = true;
        it->_kids[0]->_red = it->_kids[1]->_red = false;
      }
      if ( is_red(it) && is_red(par) ){
        uint dir2 = (tmp->_kids[1] == gpar);
        if ( it == par->_kids[last] )
          tmp->_kids[dir2] = rotate(gpar, 1-last);
        else
          tmp->_kids[dir2] = double_rotate(gpar, 1-last);
      }
      // found
      if ( it->_data == data ){
        inserted = it;
        break;
      }
      last = direction;
      direction = (it->_data < data);
      if ( gpar != 0 ) 
        tmp = gpar;
      gpar = par;
      par = it;
      it = it->_kids[direction];
    }
    _root = head._kids[1];
  }
  _root->_red = false;
  return inserted->_data;
}

template <class Data>
inline
Data & Lifoset<Data>::insert2(const Data & data)
{
  Node * inserted(0);
  _root = insert_node(inserted,_root,data);
  _root->_red = false;
  return inserted->_data;
}

template <class Data>
inline
typename Lifoset<Data>::Node * Lifoset<Data>::insert_node(Node*& inserted, Node* root, const Data & data)
{
  if ( root == 0 )
    inserted = root = make_node( data );
  else if ( root->_data == data )
    inserted = root;
  else {
    uint direction = ( root->_data < data ), dir1 = 1-direction;
    Node *& kiddir = root->_kids[direction];
    kiddir = insert_node(inserted, kiddir, data);
    
    if ( is_red(kiddir) ){
      if ( is_red(root->_kids[dir1]) ){
        // case 1
        root->_red = true;
        root->_kids[0]->_red = root->_kids[1]->_red = false;
      } else if ( is_red(kiddir->_kids[direction]) )
        // case 2
        root = rotate(root, dir1);
      else if ( is_red(kiddir->_kids[dir1]) )
        // case 3
        root = double_rotate(root, dir1);
    }
  }
  return root;
}

template <class Data>
inline
typename Lifoset<Data>::Node * Lifoset<Data>::make_node(const Data& data)
{
  lifodat.push_back(Node(data));
  ++_size;
  return &lifodat.back();
}

template <class Data>
inline
typename Lifoset<Data>::Node* Lifoset<Data>::rotate(Node* root, uint direction)
{
  Node * save = root->_kids[1-direction];
  if (!save) std::cout << "not save" << std::endl;
  root->_kids[1-direction] = save->_kids[direction];
  save->_kids[direction] = root;
  root->_red = true;
  save->_red = false;
  
  return save;
}

template <class Data>
inline
typename Lifoset<Data>::Node* Lifoset<Data>::double_rotate(Node* root, uint direction)
{
  root->_kids[1-direction] = rotate(root->_kids[1-direction],1-direction);
  return rotate(root, direction);
}

template <class Data>
uint Lifoset<Data>::rb_assert()
{
  return rb_assert(_root);
}
template <class Data>
uint Lifoset<Data>::rb_assert( Node * root )
{
  uint lh, rh;
  if (root == 0) return 1;
  Node 
    * ln = root->_kids[0],
    * rn = root->_kids[1];
  // test for consecutive red links
  if ( is_red(root) ){
    if ( is_red(ln) || is_red(rn) ){
      std::cerr << "Red violation!" << std::endl;
      return 0;
    }
  }
  lh = rb_assert(ln);
  rh = rb_assert(rn);
  // test for invalid binary search tree
  if (  (ln != 0 && !(ln->_data < root->_data) )
      ||(rn != 0 && (rn->_data < root->_data || rn->_data == root->_data ) ) ){
    std::cerr << "Binary tree violation!" << std::endl;
    return 0;
  }
  // test for black height mismatch
  if ( lh != 0 && rh != 0 && lh != rh ){
    std::cerr << "Black violation!" << std::endl;
    return 0;
  }
  // counts black links
  if ( lh != 0 && rh != 0 )
    return is_red(root) ? lh : lh + 1;
  else
    return 0;
}
