// Red-Black Tree set structure on LIFO (last-in, first-out) stack
// based on tutorial of Julienne Walker at http://www.eternallyconfuzzled.com/tuts/datastructures/jsw_tut_rbtree.aspx
#ifndef LIFOSET
#define LIFOSET

#include <utility> // for std::pair
#include <list> // example of LIFO data structure
#include <ostream> // for error messages in the test function

// TODO: REPLACE WITH THE PROPER LIFO
// simple LIFO data structure
template <class T>
struct LIFODat : public std::list<T>{};

// TODO: REPLACE WITH THE PROPER DATA TYPE (must have operator< and operator==) 
typedef int LSData;

typedef unsigned int uint;
/// Red-Black Tree map structure on LIFO (last-in, first-out) stack
template <class Data>
struct Lifoset {
  Lifoset<Data> () : _root(0), _size(0){};
  Data & insert(const Data& data);
  Data & insert2(const Data& data);
  size_t size() const { return _size; };
  // test function
  uint rb_assert();
  private:
  struct Node {
    Node() : _red(true), _data(0){ _kids[0] = _kids[1] = 0; };
    Node(const Data & data) : _red(true), _data(data){ _kids[0] = _kids[1] = 0; };
    bool _red;
    Node * _kids[2];
    Data _data;
  };
  bool is_red( const Node * root ) const { return (root != 0 && root->_red); };
  // TODO: REPLACE WITH THE PROPER LIFO
  Node * make_node( const Data & data );
  Node * rotate( Node * root, uint direction );
  Node * double_rotate( Node * root, uint direction );
  Node * insert_node( Node*& inserted, Node * root, const Data & data );
  // test function
  uint rb_assert( Node * root );
  // TODO: REPLACE WITH THE PROPER LIFO
  // LIFO data structure
  LIFODat<Node> lifodat;
  Node * _root; 
  size_t _size;
};


#include "lifoset.cxx"
#endif
