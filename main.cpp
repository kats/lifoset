#include <iostream>
#include <set> 
#include <time.h>
#include <stdlib.h> 
#include "lifoset.h"

using namespace std;

int main() {
  time_t start,end;
  
  srand(123456);
  time (&start);
  Lifoset<LSData> lsset;
  for ( uint i = 0; i < 10000000; ++i ){
    int nn = rand()%10000000+1;
    lsset.insert(nn);
  }
  time (&end);
  cout << "size of lifoset: " << lsset.size() << ", " << difftime(end,start) << " sec" << std::endl;
  cout << "assert: " << lsset.rb_assert() << endl;
  
  srand(123456);
  time (&start);
  Lifoset<LSData> lsset2;
  for ( uint i = 0; i < 10000000; ++i ){
    int nn = rand()%10000000+1;
    lsset2.insert2(nn);
  }
  time (&end);
  cout << "size of lifoset (recursive insert): " << lsset2.size() << ", " << difftime(end,start) << " sec" << std::endl;
  cout << "assert: " << lsset2.rb_assert() << endl;
  
  srand(123456);
  time (&start);
  std::set<LSData> sset;
  
  for ( uint i = 0; i < 10000000; ++i ){
    int nn = rand()%10000000+1;
    sset.insert(nn);
  }
  time (&end);
  cout << "size of std::set: " << sset.size() << ", " << difftime(end,start) << " sec" << std::endl;
  return 0;
}
